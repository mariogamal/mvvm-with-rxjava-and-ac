package com.vavisa.acdemo.Base;

import android.support.annotation.LayoutRes;

import dagger.android.support.DaggerAppCompatActivity;

public abstract class BaseActivity extends DaggerAppCompatActivity {

    @LayoutRes
    protected abstract int layoutRes();

}
