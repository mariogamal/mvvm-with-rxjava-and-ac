package com.vavisa.acdemo.Network;

import static com.vavisa.acdemo.Network.Status.ERROR;
import static com.vavisa.acdemo.Network.Status.LOADING;
import static com.vavisa.acdemo.Network.Status.SUCCESS;

public class ApiResponse<T> {

    public final Status status;

    private final T data;

    public final Throwable error;

    private ApiResponse(Status status, T data, Throwable error) {
        this.status = status;
        this.data = data;
        this.error = error;
    }

    public static <T> ApiResponse<T> loading() {
        return new ApiResponse<>(LOADING, null, null);
    }

    public static <T> ApiResponse<T> success(T data) {
        return new ApiResponse<>(SUCCESS, data, null);
    }

    public static <T> ApiResponse<T> error(Throwable error) {
        return new ApiResponse<>(ERROR, null, error);
    }

    public T getData() {
        return data;
    }
}
