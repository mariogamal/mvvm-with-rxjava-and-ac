package com.vavisa.acdemo.Network;

import com.vavisa.acdemo.Main.Models.Repo;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GithubService {
    String ENDPOINT = "https://api.github.com";

    @GET("/users/{user}/repos")
    Observable<List<Repo>> reposForUser(@Path("user") String user);
}