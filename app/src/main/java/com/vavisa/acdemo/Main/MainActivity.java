package com.vavisa.acdemo.Main;

import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.vavisa.acdemo.Base.BaseActivity;
import com.vavisa.acdemo.Main.Models.Repo;
import com.vavisa.acdemo.Network.ApiResponse;
import com.vavisa.acdemo.R;
import com.vavisa.acdemo.Utils.Constant;
import com.vavisa.acdemo.Utils.ViewModelFactory;

import java.util.List;

import javax.inject.Inject;

import static com.vavisa.acdemo.Network.Status.ERROR;
import static com.vavisa.acdemo.Network.Status.LOADING;
import static com.vavisa.acdemo.Network.Status.SUCCESS;

public class MainActivity extends BaseActivity {

    MainViewModel viewModel;
    ProgressDialog progressDialog;

    @Inject
    ViewModelFactory viewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = Constant.getProgressDialog(this, "Please wait...");
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel.class);
        viewModel.searchResponse().observe(this, this::consumeResponse);
        searchClicked();
    }

    private void searchClicked(){
        if (!Constant.checkInternetConnection(this)) {
            Toast.makeText(this,getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
        } else {
            viewModel.hitSearchApi("square");
        }
    }

    private void consumeResponse(ApiResponse<List<Repo>> apiResponse) {
        switch (apiResponse.status) {

            case LOADING:
                progressDialog.show();
                break;

            case SUCCESS:
                progressDialog.dismiss();
                renderSuccessResponse(apiResponse.getData());
                break;

            case ERROR:
                progressDialog.dismiss();
                Log.d("ErrorResponse", apiResponse.error.getMessage());
                Toast.makeText(this, getResources().getString(R.string.errorString), Toast.LENGTH_SHORT).show();
                break;

            default:
                break;
        }
    }

    private void renderSuccessResponse(List<Repo> data) {
        Toast.makeText(this,data.size()+"", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected int layoutRes() {
        return R.layout.activity_main;
    }
}
