package com.vavisa.acdemo.Main;

import com.vavisa.acdemo.Main.Models.Repo;
import com.vavisa.acdemo.Network.GithubService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

class MainRepository {

    private final GithubService githubService;

    @Inject
    MainRepository(GithubService githubService) {
        this.githubService = githubService;
    }

    Observable<List<Repo>> fetchGitHub(String user) {

        //1. Note how we're using the exact object type returned from Retrofit here
        //Observable<List<Repo>> reposReturnedObservable = githubService.reposForUser(user);

        return githubService.reposForUser(user);
    }
}
