package com.vavisa.acdemo.Main.Models;

import com.google.gson.annotations.Expose;

public class Repo {
    @Expose
    private int id;

    @Expose
    private String name;

    @Expose
    private String html_url;

    @Expose
    private Owner owner;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return html_url;
    }

    public Owner getOwner() {
        return owner;
    }
}