package com.vavisa.acdemo.Main.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Owner {

    @Expose
    private int id;

    @Expose
    private String login;

    @Expose
    private String url;

    @SerializedName("avatar_url")
    private String imageUrl;

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getUrl() {
        return url;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}