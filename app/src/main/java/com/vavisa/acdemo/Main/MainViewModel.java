package com.vavisa.acdemo.Main;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.vavisa.acdemo.Main.Models.Repo;
import com.vavisa.acdemo.Network.ApiResponse;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel extends ViewModel {

    private MainRepository repository;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final MutableLiveData<ApiResponse<List<Repo>>> responseLiveData = new MutableLiveData<>();

    @Inject
    public MainViewModel(MainRepository repository) {
        this.repository = repository;
    }

    MutableLiveData<ApiResponse<List<Repo>>> searchResponse() {
        return responseLiveData;
    }

    void hitSearchApi(String username) {
        disposables.add(repository.fetchGitHub(username)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe((d) -> responseLiveData.setValue(ApiResponse.loading()))
                .subscribe(
                        result -> responseLiveData.setValue(ApiResponse.success(result)),
                        throwable -> responseLiveData.setValue(ApiResponse.error(throwable))
                ));
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }
}
