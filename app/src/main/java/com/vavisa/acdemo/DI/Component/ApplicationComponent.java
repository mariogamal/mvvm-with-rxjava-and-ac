package com.vavisa.acdemo.DI.Component;

import android.app.Application;

import com.vavisa.acdemo.Base.BaseApplication;
import com.vavisa.acdemo.DI.Module.ActivityBindingModule;
import com.vavisa.acdemo.DI.Module.ApplicationModule;
import com.vavisa.acdemo.DI.Module.ContextModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;

@Singleton
@Component(modules = {ContextModule.class, ApplicationModule.class, AndroidSupportInjectionModule.class, ActivityBindingModule.class})
public interface ApplicationComponent extends AndroidInjector<DaggerApplication> {

    void inject(BaseApplication application);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        ApplicationComponent build();
    }
}